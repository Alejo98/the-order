import {
  HeroContainer,
  HeroContent,
  HeroItems,
  HeroH1,
  HeroP,
  HeroBtn
} from './HeroElements';
import React, { useState } from 'react';
import Navbar from '../Navbar';
import Sidebar from '../Sidebar';


// class Index extends Component {
//   constructor(props) {
//     [isOpen, setIsOpen] = useState(false);
//     super(props);
//     this.state = initialState;

 
//   }

//   setIsOpen = () => {
//     this.setState({
//       isOpen: !this.state.isOpen
//     });
//   }

//   toggle = () => {
//     setIsOpen(!isOpen);
//   };

//   toggleModal = () => {
//     this.setState({
//       isOpen: !this.state.isOpen
//     });
//   }

//   toggleModalClose = () => {  // modal close to reset input val
//     this.setState(initialState);
//   }

//   handleChange = (e) => {
//     this.setState({
//       value: e.target.value
//     });
//   }

//   render() {
//     return (
//       <HeroContainer>
//       <Navbar toggle={toggle} />
//       <Sidebar isOpen={isOpen} toggle={toggle} />
//       <HeroContent>
//         <HeroItems>
          
//           <HeroH1>Greatest Pizza Ever</HeroH1>
//           <HeroP>Ready in 60 seconds</HeroP>
//           <HeroBtn><div className="Index">
//         <button onClick={this.toggleModal}>
//           Open the modal
//         </button>

//         <Modal show={this.state.isOpen}
//           onClose={this.toggleModalClose}>
//           <input type="text" value={this.state.value} onChange={this.handleChange} /><br/><br/>
//         </Modal>
//       </div></HeroBtn>
//         </HeroItems>
//       </HeroContent>
//     </HeroContainer>
//     );
//   }
// }

// render(<Index />, document.getElementById('root'));

export const Hero = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  };

  return (
    <HeroContainer>
      <Navbar toggle={toggle} />
      <Sidebar isOpen={isOpen} toggle={toggle} />
      <HeroContent>
        <HeroItems>
          <HeroH1>Greatest Pizza Ever</HeroH1>
          <HeroP>Ready in 60 seconds</HeroP>
          <HeroBtn>Place Order</HeroBtn>
        </HeroItems>
      </HeroContent>
    </HeroContainer>
  );
};

export default Hero;
